package Principal;

public class telaPrincipal extends javax.swing.JFrame {
    
    public analiseLexica analise;
    
    public telaPrincipal() {
        initComponents();
        String analiseL = "";
        analise = new analiseLexica();
        this.setLocationRelativeTo(null);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scroll = new javax.swing.JScrollPane();
        cxDeTexto = new javax.swing.JTextArea();
        anLexico = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Compilador Python");
        setBackground(new java.awt.Color(0, 153, 204));

        cxDeTexto.setColumns(20);
        cxDeTexto.setRows(5);
        scroll.setViewportView(cxDeTexto);

        anLexico.setText("Análise Léxica");
        anLexico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anLexicoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scroll, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(anLexico)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(anLexico)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void anLexicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anLexicoActionPerformed
        String codigo = cxDeTexto.getText();
        this.setVisible(false);
        new telaExibicao(analise.percorreCod(codigo)).setVisible(true);
    }//GEN-LAST:event_anLexicoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
                
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new telaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton anLexico;
    private javax.swing.JTextArea cxDeTexto;
    private javax.swing.JScrollPane scroll;
    // End of variables declaration//GEN-END:variables
}
