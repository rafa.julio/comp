package Principal;


public class analiseLexica {
    
    
    String[] pReservadas = {
        "and","as","assert","break",
        "class","continue","def","del",
        "elif","else","except","exec",
        "finally","for","from","global",
        "if","import","in","is",
        "lambda","not","or","pass",
        "print","raise","return","try",
        "while","with","yield","true","false"
    };
    String[] variaveis = {
        "str","unicode","list","tuple",
        "set","frozenset","dict","int",
        "float","complex","bool"
    };
    String[] oAritimeticos = {
        "+","-","*","/","**","//","%"  
    };
    String[] oRelacionais = {
        ">","<","==",">=","<="  
    };
    
    String[] agrupadores = {
        "(",")"
    };
    
    public String percorreCod(String codigo){
        String lexica = "";
        String analise = "";
        String palavra = "";
        for (int i = 0; i < codigo.length(); i++) {
            char p = codigo.charAt(i);
            switch(p){
                case ' ':
                case '\n':
                case '\t':
                    //analise = analise + reservadas(palavra);
                    lexica = lexica + compara(analise,palavra);
                    palavra = "";
                    break;
                case '(':
                    lexica = lexica + "( - Agrupador\n";
                    palavra = "";
                    break;
                case ')':
                    lexica = lexica + ") - Agrupador\n";
                    palavra = "";
                    break;
                default:
                    palavra = palavra + new StringBuilder().append(p).toString();
                    break;
            }
        }
        return lexica;
    }
    
    public String compara(String analise,String palavra){
        for (int i = 0; i < pReservadas.length; i++) {
            if(palavra.equals(pReservadas[i])){
                analise = analise + palavra + " - Palavra Reservada\n";
                break;
            }
        }
        
        for (int i = 0; i < oAritimeticos.length; i++) {
            if(palavra.equals(oAritimeticos[i])){
                analise = analise + palavra +  " - Operador Aritimetico\n";
                break;
            }
        }
        return analise;
    }
          
    /*public String reservadas(String palavra){
        String p = "";
        for (int i = 0; i <= pReservadas.length; i++) {
            if(palavra.equals(pReservadas[i])){
                p = palavra;
                break;
            }
        }
        return p + " - Palavra Reservada\n";
    }*/
    
    public String oAritimeticos(String palavra){
        String p = "";
        for (int i = 0; i <= oAritimeticos.length; i++) {
            if(palavra.equals(oAritimeticos[i])){
                p = palavra;
                break;
            }
        }
        return p + " - Operador Aritimético\n";
    }
    
    
    
}
